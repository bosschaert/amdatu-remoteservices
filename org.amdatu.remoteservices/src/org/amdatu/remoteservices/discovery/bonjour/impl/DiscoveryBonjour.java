package org.amdatu.remoteservices.discovery.bonjour.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import org.amdatu.remoteservices.discovery.commands.Discovery;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

public class DiscoveryBonjour implements EndpointListener, ServiceListener, Discovery {
	
	private static final String SERVICE_TYPE = "_http._tcp.local.";
	
	private BundleContext m_bundleContext;
	
	private Map<ServiceReference, EndpointListener> m_listeners = new HashMap<ServiceReference, EndpointListener>();

	private ServiceInfo m_si;

	private JmDNS m_jmDNS;

	private Map<ServiceInfo, EndpointDescription> m_importedEndpoints = new HashMap<ServiceInfo, EndpointDescription>();
	private Map<EndpointDescription, ServiceInfo> m_exportedEndpoints = new HashMap<EndpointDescription, ServiceInfo>();
	
	public void start() {
		try {
			m_jmDNS = JmDNS.create();
			m_jmDNS.addServiceListener("_http._tcp.local.", this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void stop() {
		if (m_si != null) {
			m_jmDNS.unregisterAllServices();
			m_jmDNS.removeServiceListener("_http._tcp.local.", this);
		}
		
	}

	@Override
	public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
		System.out.println("Bonjour: add endpoint: " + endpoint.toString());
		try {
			String service = endpoint.getId().substring(endpoint.getFrameworkUUID().length());
			
			Map<String, Object> props = new HashMap<String, Object>();
			
			props.put("path", service);
			
	        for (String key : endpoint.getProperties().keySet()) {
				if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
					String value = join(endpoint.getConfigurationTypes(), ",");
					props.put(key, value);
				} else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
					props.put(key, endpoint.getFrameworkUUID());
				} else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
					props.put(key, endpoint.getId());
				} else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
					props.put(key, String.valueOf(endpoint.getServiceId()));
				} else if (Constants.OBJECTCLASS.equals(key)) {
					props.put(key, join(endpoint.getInterfaces(), ","));
				} else if (key.equals(Constants.SERVICE_ID)){
					props.put(key, String.valueOf(endpoint.getServiceId()));
				} else { 
					Object obj = endpoint.getProperties().get(key);
					if (obj instanceof Integer) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof String) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof Integer) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof Boolean) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof Character) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof Short) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof Long) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof Float) {
						props.put(key, String.valueOf(obj));
					} else if (obj instanceof Double) {
						props.put(key, String.valueOf(obj));
					} else {
						System.out.println("Ignore non-primitive types due to java-c mapping.");
//						props.put(key, toByteArray(obj));
					}
				}
			}
			
	        String port = (String) endpoint.getProperties().get(".ars.port");
			m_si = ServiceInfo.create(SERVICE_TYPE, "OSGi Remote Service Admin Endpoint", "_osgi", Integer.parseInt(port), 0, 0, props);
			m_jmDNS.registerService(m_si);
			
			m_exportedEndpoints.put(endpoint, m_si);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public byte[] toByteArray (Object obj)
	{
	  byte[] bytes = null;
	  ByteArrayOutputStream bos = new ByteArrayOutputStream();
	  try {
	    ObjectOutputStream oos = new ObjectOutputStream(bos); 
	    oos.writeObject(obj);
	    oos.flush(); 
	    oos.close(); 
	    bos.close();
	    bytes = bos.toByteArray ();
	  }
	  catch (IOException ex) {
	    //TODO: Handle the exception
	  }
	  return bytes;
	}
	
	public Object toObject (byte[] bytes)
	{
	  Object obj = null;
	  try {
	    ByteArrayInputStream bis = new ByteArrayInputStream (bytes);
	    ObjectInputStream ois = new ObjectInputStream (bis);
	    obj = ois.readObject();
	  }
	  catch (IOException ex) {
	    //TODO: Handle the exception
	  }
	  catch (ClassNotFoundException ex) {
	    //TODO: Handle the exception
	  }
	  return obj;
	}
	
	public String join(Iterable<? extends CharSequence> s, String delimiter) {
	    Iterator<? extends CharSequence> iter = s.iterator();
	    if (!iter.hasNext()) return "";
	    StringBuilder buffer = new StringBuilder(iter.next());
	    while (iter.hasNext()) buffer.append(delimiter).append(iter.next());
	    return buffer.toString();
	}

	@Override
	public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
		System.out.println("Bonjour: remove endpoint: " + endpoint.toString());
		ServiceInfo serviceInfo = m_exportedEndpoints.remove(endpoint);
		m_jmDNS.unregisterService(serviceInfo);
	}

	public void endpointListenerAdded(ServiceReference reference, EndpointListener listener) {
		Boolean isDiscovery = (Boolean) reference.getProperty("discovery");
		if (isDiscovery == null) {
			isDiscovery = false;
		}
		if (!isDiscovery) {
			m_listeners.put(reference, listener);
			
			for (EndpointDescription description : m_importedEndpoints.values()) {
				informListenerOfAddition(reference, description);
			}
		}
	}
	
	public void endpointListenerModified(ServiceReference reference, EndpointListener listener) {
		Boolean isDiscovery = (Boolean) reference.getProperty("discovery");
		if (isDiscovery == null) {
			isDiscovery = false;
		}
		if (!isDiscovery) {
			m_listeners.put(reference, listener);
		}
	}		
	
	public void endpointListenerRemoved(ServiceReference reference, EndpointListener listener) {
		m_listeners.remove(reference);
	}
	
	private void informListenersOfAddition(EndpointDescription description) {
		for (ServiceReference reference : m_listeners.keySet()) {
			informListenerOfAddition(reference, description);
		}
	}
	
	private void informListenerOfAddition(ServiceReference reference, EndpointDescription description) {
		EndpointListener listener = m_listeners.get(reference);

		String[] scopes = propertyToStringArray(reference.getProperty(ENDPOINT_LISTENER_SCOPE));
		for (String string : scopes) {
			try {
				Filter filter = m_bundleContext.createFilter(string);
				Dictionary<String, Object> properties = new Hashtable<String, Object>(description.getProperties());
				if (filter != null && filter.match(properties)) {
					listener.endpointAdded(description, filter.toString());
				}
			} catch (InvalidSyntaxException e) {
				System.out.println("DISCOVERY: Syntax incorrect, scope ignored: " + e.getMessage());
			}
		}
	}
	
	private void informListenersOfRemoval(EndpointDescription description) {
 		for (ServiceReference reference : m_listeners.keySet()) {
			EndpointListener listener = m_listeners.get(reference);
			
			String[] scopes = propertyToStringArray(reference.getProperty(ENDPOINT_LISTENER_SCOPE));
			for (String string : scopes) {
				try {
					Filter filter = m_bundleContext.createFilter(string);
					Dictionary<String, Object> properties = new Hashtable<String, Object>(description.getProperties());
					if (filter != null && filter.match(properties)) {
						listener.endpointRemoved(description, filter.toString());
					}
				} catch (InvalidSyntaxException e) {
				}
			}
			
		}
	}
	
	private String[] propertyToStringArray(Object property) {

        if (property instanceof String) {
            // System.out.println("String");
            String[] ret = new String[1];
            ret[0] = (String)property;
            return ret;
        }

        if (property instanceof String[]) {
            // System.out.println("String[]");
            return (String[])property;
        }

        if (property instanceof Collection) {
            Collection<?> col = (Collection<?>) property;
            // System.out.println("Collection: size "+col.size());
            String[] ret = new String[col.size()];
            int x = 0;
            for (Object s : col) {
                ret[x] = (String)s;
                ++x;
            }
            return ret;
        }

        return new String[0];
    }

	@Override
	public void serviceAdded(ServiceEvent arg0) {
	}

	@Override
	public void serviceRemoved(ServiceEvent arg0) {
		System.out.println("Bonjour: Removed service: " + arg0.getInfo().getNiceTextString());
		EndpointDescription removed = m_importedEndpoints.remove(arg0.getInfo());
		informListenersOfRemoval(removed);
	}

	@Override
	public void serviceResolved(ServiceEvent arg0) {
		String subtype = arg0.getInfo().getSubtype();
		if (subtype.equals("osgi")) {
			System.out.println("Bonjour: Resolved new service: " + arg0.getInfo().getURL());
			Hashtable<String, Object> descriptionProps = new Hashtable<String, Object>();
			Enumeration<String> propertyNames = arg0.getInfo().getPropertyNames();
			while (propertyNames.hasMoreElements()) {
				String key = (String) propertyNames.nextElement();
				String value = arg0.getInfo().getPropertyString(key);
				
				if (RemoteConstants.SERVICE_IMPORTED_CONFIGS.equals(key)) {
					String[] configurations = value.split(",");
					descriptionProps.put(RemoteConstants.SERVICE_IMPORTED_CONFIGS, configurations);
				} else if (RemoteConstants.ENDPOINT_FRAMEWORK_UUID.equals(key)) {
					String uuid = value;
					descriptionProps.put(RemoteConstants.ENDPOINT_FRAMEWORK_UUID, uuid);
				} else if (RemoteConstants.ENDPOINT_ID.equals(key)) {
					String endpointId = value;
					descriptionProps.put(RemoteConstants.ENDPOINT_ID, endpointId);
				} else if (RemoteConstants.ENDPOINT_SERVICE_ID.equals(key)) {
					Long serviceId = Long.valueOf(value);
					if (serviceId != null) {
						descriptionProps.put(RemoteConstants.ENDPOINT_SERVICE_ID, serviceId);
					}
				} else if (Constants.OBJECTCLASS.equals(key)) {
					String[] objectClasses = value.split(",");
					descriptionProps.put(Constants.OBJECTCLASS, objectClasses);
				} else if (key.equals(Constants.SERVICE_ID)){
					Long serviceId = Long.valueOf(value);
					if (serviceId != null) {
						descriptionProps.put(Constants.SERVICE_ID, serviceId);
					}
				} else { 
					descriptionProps.put(key, value);
				}
			}
			
			EndpointDescription endpointDescription = new EndpointDescription(descriptionProps);
			informListenersOfAddition(endpointDescription);
	        m_importedEndpoints .put(arg0.getInfo(), endpointDescription);
		}
	}

	@Override
	public List<EndpointDescription> getImportedEndpoints() {
		List<EndpointDescription> imports = new ArrayList<EndpointDescription>();
		imports.addAll(m_importedEndpoints.values());
		return imports;
	}

	@Override
	public List<EndpointDescription> getExportedEndpoints() {
		List<EndpointDescription> exports = new ArrayList<EndpointDescription>();
		exports.addAll(m_exportedEndpoints.keySet());
		return exports;
	}

}
