package org.amdatu.remoteservices.discovery.slp.da.impl;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Properties;

import org.livetribe.slp.da.StandardDirectoryAgentServer;
import org.livetribe.slp.settings.PropertiesSettings;
import org.livetribe.slp.spi.Server;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

public class DirectoryAgent implements ManagedService {
	
	private Server m_server;

	@Override
	public void updated(Dictionary properties) throws ConfigurationException {
		Properties props = new Properties();
		Enumeration keys = properties.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = (String) properties.get(key);
			props.setProperty(key, value);
		}
		ClassLoader old = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
		PropertiesSettings das = PropertiesSettings.from(props);
		m_server = StandardDirectoryAgentServer.newInstance(das);
		m_server.start();
		Thread.currentThread().setContextClassLoader(old);
	}
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void stop() {
		if (m_server.isRunning()) {
			m_server.stop();
		}
	}
	
}
