package org.amdatu.remoteservices.discovery.slp.da.impl;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		manager.add(createComponent()
			.setImplementation(DirectoryAgent.class)
			.add(createConfigurationDependency()
				.setPid("org.amdatu.remoteservices.discovery.slp.da"))
			);
}
	
	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception {
	}

}
