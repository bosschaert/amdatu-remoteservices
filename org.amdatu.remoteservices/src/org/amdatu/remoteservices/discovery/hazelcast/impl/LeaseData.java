package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.io.Serializable;

public class LeaseData implements Serializable {

	private static final long serialVersionUID = 3638189340795255245L;
	
	private String m_id;
	private long m_duration;
	
	public LeaseData(String id, long duration) {
		m_id = id;
		m_duration = duration;
	}
	
	public String getId() {
		return m_id;
	}
	
	public long getDuration() {
		return m_duration;
	}
}
