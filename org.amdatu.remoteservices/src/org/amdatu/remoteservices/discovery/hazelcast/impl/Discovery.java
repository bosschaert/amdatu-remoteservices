package org.amdatu.remoteservices.discovery.hazelcast.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointListener;

import com.hazelcast.config.Config;
import com.hazelcast.config.Join;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.Member;

/**
 * HazelCast based discovery for OSGi RemoteServiceAdmin with lease support.
 * 
 * This implementation uses HazelCast to publish and discover remote services. To handle remote services a distributed map is used.
 * Leases are also stored in a distributed map. They are handled by two threads, one renews leases, the other expires leases. 
 * Renewal of a lease is handled via a Topic and messages.
 * 
 * @author alexander
 *
 */
public class Discovery implements EndpointListener, EntryListener<String, EndpointData>, org.amdatu.remoteservices.discovery.commands.Discovery {
	
	public static String LEASES_MAP = "leases.map";
	public static String ENDPOINTS_MAP = "endpoints.map";
	
	public static String LEASE_TOPIC = "lease.topic";
	
	private static final String ARS_CONFIG_TYPE = ".ars"; // Amdatu Remote Service service type
	private static final String ARS_ALIAS = ARS_CONFIG_TYPE + ".alias"; // ARS alias

	private volatile BundleContext m_bundleContext;
	private volatile LogService m_log;

	private Map<ServiceReference, EndpointListener> m_listeners = new HashMap<ServiceReference, EndpointListener>();

	private Map<String, EndpointDescription> m_importedEndpoints = new HashMap<String, EndpointDescription>();
	private Map<String, Lease> m_importLeases = new HashMap<String, Lease>();
	
	private Map<EndpointDescription, EndpointData> m_exportedEndpoints = new HashMap<EndpointDescription, EndpointData>();
	private Map<String, Lease> m_exportLeases = new HashMap<String, Lease>();
	
	private IMap<String, EndpointData> m_endpointdata;
	private IMap<String, LeaseData> m_leases;
	
	private HazelcastInstance m_hazelcast;

	private LeaseRenewal m_leaseRenewal;

	private LeaseExpiration m_leaseExpiration;
	
	/**
	 * Set up the required hazelcast instance, lease handling threads etc.
	 * During startup all current entries in the distributed map are also handled.
	 */
	public void start() {
//		Config config = new Config();
//		config.setClassLoader(this.getClass().getClassLoader());
//		config.setProperty("hazelcast.logging.type", "none");
//		
//		String ifProperty = m_bundleContext.getProperty("discovery.interfaces");
//		if (ifProperty != null)  {
//			String[] ifs = ifProperty.split(" ");
//			final NetworkConfig networkConfig = new NetworkConfig();
//			Interfaces interfaces = new Interfaces();
//			for (String inteface : ifs) {
//				interfaces.addInterface(inteface);
//			}	
//			interfaces.setEnabled( true );
//			networkConfig.setInterfaces( interfaces );
//			config.setNetworkConfig(networkConfig);
//		}
		
		
		Config config = new Config();
		config.setClassLoader(this.getClass().getClassLoader());
		config.setProperty("hazelcast.logging.type", "none");
		config.setPort(5900);
		        
		NetworkConfig network = config.getNetworkConfig();
		Join join = network.getJoin();
		String tcpipProperty = m_bundleContext.getProperty("discovery.tcpip");
		if (tcpipProperty != null) {
		    join.getMulticastConfig().setEnabled(false);
		    TcpIpConfig tcpip = join.getTcpIpConfig();
		    String[] members = tcpipProperty.split(" ");
		    for (String member : members) {
		        tcpip.addMember(member);
		    }
		    tcpip.setEnabled(true);
		}

//		network.getInterfaces().setEnabled(true).addInterface("192.168.2.*");
		
		m_hazelcast = Hazelcast.newHazelcastInstance(config);
		
		m_leaseRenewal = new LeaseRenewal(m_hazelcast, m_exportLeases);
		m_leaseExpiration = new LeaseExpiration(m_hazelcast, m_importLeases);
		
		m_endpointdata = m_hazelcast.getMap(ENDPOINTS_MAP);
		m_endpointdata.addEntryListener(this, true);
		
		m_leases = m_hazelcast.getMap(LEASES_MAP);
		
		ClassLoader ccl = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
		
		for (String key : m_endpointdata.keySet()) {
			EndpointData data = m_endpointdata.get(key);
			String hostName = data.getOwner().getInetSocketAddress().getHostName();
			System.out.println("Name: " + hostName);
			EndpointDescription description = new EndpointDescription(data.getProperties());
			description.getId();
			System.out.println("ALIAS: " + description.getProperties().get(ARS_ALIAS));
			m_importedEndpoints.put(key, description);
			
			LeaseData leaseData = m_leases.get(data.getId());
			Lease lease = new Lease(data.getId(), leaseData.getDuration());
			m_importLeases.put(data.getId(), lease);
			
			informListenersOfAddition(description);
		}
		
		Thread.currentThread().setContextClassLoader(ccl);
	}
	
	public void stop() {
		m_leaseRenewal.stop();
		m_leaseExpiration.stop();
		
		// Remove all exported endpoints made by this component
		ClassLoader ccl = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
		for (Entry<EndpointDescription, EndpointData> entry : m_exportedEndpoints.entrySet()) {
			System.out.println("Remove entry");
	        EndpointData data = entry.getValue();
	        m_endpointdata.remove(data.getId(), data);
	        m_exportLeases.remove(data.getId());
	        m_leases.remove(data.getId());
		}
		Thread.currentThread().setContextClassLoader(ccl);
		
		m_hazelcast.getLifecycleService().shutdown();
	}
	
	/** Shell command to list all members in the cluster. */
	public void members() {
		String header = m_bundleContext.getBundle().getSymbolicName() + " [" + m_bundleContext.getBundle().getBundleId() + "] cluster members:";
		System.out.println(header);
        StringBuilder underline = new StringBuilder();
        for (int i = 0; i < header.length(); i++) {
        	underline.append("-");
        }
        System.out.println(underline);
        
	    Set<Member> members = m_hazelcast.getCluster().getMembers();
	    if (members.isEmpty()) {
	        System.out.println("   [EMPTY]");
	    }
	    else {
	        for (Member member : members) {
	            System.out.println("   " + member);
	        }
	    }
	}

	/**
	 * When an endpoint is added a new EndpointData object is created. This object is stored in the distributed endpoint map.
	 * Also a new LeaseData object is created, leasetime is set to the leasetime specified in endpoint, or to the default 60 seconds.
	 */
	@Override
	public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
		m_log.log(LogService.LOG_INFO, "Publish exported endpoint '" + endpoint.getId() + "' for matched filter '" + matchedFilter + "'");
		ClassLoader ccl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        
        Member localMember = m_hazelcast.getCluster().getLocalMember();
		EndpointData data = new EndpointData(endpoint.getProperties(), localMember);
		m_exportedEndpoints.put(endpoint, data);
		
		// #TODO Add lease to endpointdescription
		Object leaseTime = endpoint.getProperties().get("HAZELCAST_LEASE_TIME");
		if (leaseTime == null) {
			leaseTime = new Long(60000);
		}
		
		LeaseData leaseData = new LeaseData(data.getId(), (Long) leaseTime);
		m_leases.put(data.getId(), leaseData);
		Lease lease = new Lease(data.getId(), 10);
		m_exportLeases.put(data.getId(), lease);
		
		m_endpointdata.put(data.getId(), data);
		
		Thread.currentThread().setContextClassLoader(ccl);
	}
	
	/**
	 * When an endpoint is removed its EndpointData and LeaseData need to be removed. No other actions are needed.
	 */
	@Override
	public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
		m_log.log(LogService.LOG_INFO, "Unpublish exported endpoint '" + endpoint.getId() + "' for matched filter '" + matchedFilter + "'");
		
		ClassLoader ccl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        
        EndpointData data = m_exportedEndpoints.remove(endpoint);
        m_endpointdata.remove(data.getId(), data);
        m_exportLeases.remove(data.getId());
        m_leases.remove(data.getId());
        
        Thread.currentThread().setContextClassLoader(ccl);
	}
	
	/**
	 * Callback method used by hazelcast to inform of additions to the EndpointData map.
	 * This method is used to trigger the creation for a new endpoint for the discovered remote service.
	 * The LeaseData is used to update the imported leases so that the expiration thread can handle it.
	 */
	@Override
	public void entryAdded(EntryEvent<String, EndpointData> endpointData) {
		m_log.log(LogService.LOG_INFO, "Discovered added endpoint '" + endpointData.getKey() + "' for import");
        
		System.out.println(endpointData.getValue().getOwner().getInetSocketAddress().getHostName());
		
		EndpointDescription endpoint = new EndpointDescription(endpointData.getValue().getProperties());
		m_importedEndpoints.put(endpointData.getKey(), endpoint);
		
		LeaseData leaseData = m_leases.get(endpointData.getKey());
		Lease lease = new Lease(endpointData.getKey(), leaseData.getDuration());
		m_importLeases.put(endpointData.getKey(), lease);
		
		informListenersOfAddition(endpoint);
	}

	@Override
	public void entryEvicted(EntryEvent<String, EndpointData> endpointData) {
		
	}

	/**
	 * Callback method used by hazelcast to inform of removal from the EndpointData map.
	 * This method is used to trigger the removal of an endpoint for the removed remote service.
	 * The LeaseData is removed from the imported leases.
	 * 
	 * If the system recovers from a network failure, Endpoints might have been removed by other nodes (eg via LeaseExpiration). 
	 * In this case they need to be added again.  
	 */
	@Override
	public void entryRemoved(EntryEvent<String, EndpointData> endpointData) {
        m_log.log(LogService.LOG_INFO, "Discovered removed endpoint '" + endpointData.getKey() + "' for import");
        
		EndpointDescription endpoint = m_importedEndpoints.remove(endpointData.getKey());
		m_importLeases.remove(endpointData.getKey());
		m_leases.remove(endpointData.getKey());
		
		if (endpoint != null) {
			informListenersOfRemoval(endpoint);
		}
		
		// If one of or own services is removed, but not by us, just add it again
		Member localMember = m_hazelcast.getCluster().getLocalMember();
		Member member = endpointData.getMember();
		
		if (!localMember.equals(member)) {
			for (EndpointDescription exportedEndpoint : m_exportedEndpoints.keySet()) {
				if (exportedEndpoint.getId().equals(endpointData.getKey())) {
					System.out.println("Not removed by us, are we recovering from a split brain?");
					// Readd the data
					// #TODO Add lease to endpointdescription
					EndpointData data = endpointData.getValue();
					LeaseData leaseData = new LeaseData(data.getId(), 10000);
					m_leases.put(data.getId(), leaseData);
					Lease lease = new Lease(data.getId(), 10);
					m_exportLeases.put(data.getId(), lease);
					
					m_endpointdata.put(data.getId(), data);
				}
			}
		}
	}

	@Override
	public void entryUpdated(EntryEvent<String, EndpointData> endpointData) {
		
	}

	/**
	 * Dependency callback method to handle endpoint listeners.
	 * 
	 * @param reference
	 * @param listener
	 */
	public void endpointListenerAdded(ServiceReference reference, EndpointListener listener) {
		Boolean isDiscovery = (Boolean) reference.getProperty("discovery");
		if (isDiscovery == null) {
			isDiscovery = false;
		}
		if (!isDiscovery) {
			m_listeners.put(reference, listener);
			
			for (EndpointDescription description : m_importedEndpoints.values()) {
				informListenerOfAddition(reference, description);
			}
		}
	}
	
	/**
	 * Dependency callback method to handle endpoint listeners.
	 * 
	 * @param reference
	 * @param listener
	 */
	public void endpointListenerModified(ServiceReference reference, EndpointListener listener) {
		Boolean isDiscovery = (Boolean) reference.getProperty("discovery");
		if (isDiscovery == null) {
			isDiscovery = false;
		}
		if (!isDiscovery) {
			m_listeners.put(reference, listener);
			for (EndpointDescription description : m_importedEndpoints.values()) {
				// #TODO Is this wrong? All existing endpoints are added again, and previously added onces are not removed.
				// According to the OSGi Spec 4.2 Chapter 122.6.3 the Listener implementation itself is responsible for this handling.
				// So maybe this todo can be ignored. In this case, the Listeners should handle multiple adds of the same description gracefully.
				// In this case it is also not needed to remove the descriptions first
				informListenerOfAddition(reference, description);
			}
		}
	}		
	
	/**
	 * Dependency callback method to handle endpoint listeners.
	 * 
	 * @param reference
	 * @param listener
	 */
	public void endpointListenerRemoved(ServiceReference reference, EndpointListener listener) {
		m_listeners.remove(reference);
	}
	
	private void informListenersOfAddition(EndpointDescription description) {
		for (ServiceReference reference : m_listeners.keySet()) {
			informListenerOfAddition(reference, description);
		}
	}
	
	private void informListenerOfAddition(ServiceReference reference, EndpointDescription description) {
		EndpointListener listener = m_listeners.get(reference);

		String[] scopes = propertyToStringArray(reference.getProperty(ENDPOINT_LISTENER_SCOPE));
		for (String string : scopes) {
			try {
				Filter filter = m_bundleContext.createFilter(string);
				Dictionary<String, Object> properties = new Hashtable<String, Object>(description.getProperties());
				if (filter != null && filter.match(properties)) {
					listener.endpointAdded(description, filter.toString());
				}
			} catch (InvalidSyntaxException e) {
				System.out.println("DISCOVERY: Syntax incorrect, scope ignored: " + e.getMessage());
			}
		}
	}
	
	private void informListenersOfRemoval(EndpointDescription description) {
 		for (ServiceReference reference : m_listeners.keySet()) {
 			informListenerOfRemoval(reference, description);
		}
	}
	
	private void informListenerOfRemoval(ServiceReference reference, EndpointDescription description) {
			String[] scopes = propertyToStringArray(reference.getProperty(ENDPOINT_LISTENER_SCOPE));
			informListenerOfRemoval(reference, description, scopes);
	}
	
	private void informListenerOfRemoval(ServiceReference reference, EndpointDescription description, String[] scopes) {
		EndpointListener listener = m_listeners.get(reference);
		
		for (String string : scopes) {
			try {
				Filter filter = m_bundleContext.createFilter(string);
				Dictionary<String, Object> properties = new Hashtable<String, Object>(description.getProperties());
				if (filter != null && filter.match(properties)) {
					listener.endpointRemoved(description, filter.toString());
				}
			} catch (InvalidSyntaxException e) {
			}
		}
	}
	
	private String[] propertyToStringArray(Object property) {

        if (property instanceof String) {
            String[] ret = new String[1];
            ret[0] = (String)property;
            return ret;
        }

        if (property instanceof String[]) {
            return (String[])property;
        }

        if (property instanceof Collection) {
            Collection<?> col = (Collection<?>) property;
            String[] ret = new String[col.size()];
            int x = 0;
            for (Object s : col) {
                ret[x] = (String)s;
                ++x;
            }
            return ret;
        }

        return new String[0];
    }

	@Override
	public List<EndpointDescription> getImportedEndpoints() {
		List<EndpointDescription> imports = new ArrayList<EndpointDescription>();
		imports.addAll(m_importedEndpoints.values());
		return imports;
	}

	@Override
	public List<EndpointDescription> getExportedEndpoints() {
		List<EndpointDescription> exports = new ArrayList<EndpointDescription>();
		for (EndpointDescription endpointDescription : m_exportedEndpoints.keySet()) {
			exports.add(endpointDescription);
		}
		return exports;
	}
}
