package org.amdatu.remoteservices.remoteserviceadmin.endpoint.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.HttpURLConnection;
import java.net.URL;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

public class RestClientEndpoint implements InvocationHandler {

	private final Class<?>[] interfaces;
	private final String serviceLocation;
	private final Object proxy;
	

	public RestClientEndpoint(String[] importedInterfaces, String serviceLocation) throws ClassNotFoundException {
		this.serviceLocation = serviceLocation;
		
		this.interfaces = new Class<?>[importedInterfaces.length];
		for (int i=0; i<importedInterfaces.length; i+=1) {
			this.interfaces[i] = Class.forName(importedInterfaces[i]); 
		}
		
		proxy = Proxy.newProxyInstance(getClass().getClassLoader(), interfaces, this);
	}
	
	public Object getServiceProxy() {
		return proxy;
	}
	
	@Override
	public Object invoke(Object serviceProxy, Method method, Object[] args)
			throws Throwable {
		Object result = null;
		
		URL url = new URL(serviceLocation + "/" + method.getName());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		if (method.getName().startsWith("get") && args == null) {
			result = invokeAsGet(conn, method);
		} else {
			result = invokeAsPost(conn, method, args);
		}
		return result;
	}

	private Object invokeAsGet(HttpURLConnection conn, Method method) throws IOException {
		Object result = null;
		conn.setRequestMethod("GET");
		conn.connect();
		InputStream input = conn.getInputStream();
		ObjectMapper mapper = new ObjectMapper();
		result = mapper.readValue(input, method.getReturnType());
		conn.disconnect();
		return result;
	}
	
	private Object invokeAsPost(HttpURLConnection conn, Method method, Object[] args) throws IOException {
		Object result = null;
		JsonFactory fact = new JsonFactory();
		
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.connect();
		OutputStream out = conn.getOutputStream();
		
		JsonGenerator jgen = fact.createJsonGenerator(out);
		jgen.writeStartObject();
		if (args != null) {
			for (int i=0; i<args.length; i++) {
				jgen.writeObjectField("arg" + i, args[i]);
			}
		}
		jgen.writeEndObject();
		jgen.flush();
		jgen.close();
		
		if (!method.getReturnType().equals(Void.TYPE) ) {
			ObjectMapper mapper = new ObjectMapper(fact);
			result = mapper.readValue(conn.getInputStream(), method.getReturnType());
		}
		
		if (conn.getResponseCode() != 200) {
			System.out.println("Got response code: " + conn.getResponseCode());
			System.out.println("Got response message: " + conn.getResponseMessage());
		}
		
		conn.disconnect();
		return result;
	}
}
