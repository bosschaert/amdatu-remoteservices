package org.amdatu.remoteservices.remoteserviceadmin.impl;

import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ImportReference;

public class ImportReferenceImpl implements ImportReference {
	
	private final ServiceReference m_proxyServicereference;
	private final EndpointDescription m_endpointDescription;

	ImportReferenceImpl(ServiceReference proxyServiceReference, EndpointDescription endpointDescription) {
		m_proxyServicereference = proxyServiceReference;
		m_endpointDescription = endpointDescription;
	}

	@Override
	public ServiceReference getImportedService() {
		return m_proxyServicereference;
	}

	@Override
	public EndpointDescription getImportedEndpoint() {
		return m_endpointDescription;
	}

}
