package org.amdatu.remoteservices.remoteserviceadmin.impl;

import org.osgi.framework.ServiceReference;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ExportReference;

public class ExportReferenceImpl implements ExportReference {
	
	private final ServiceReference m_serviceReference;
	private final EndpointDescription m_endpointDescription;

	public ExportReferenceImpl(ServiceReference reference, EndpointDescription endpointDescription) {
		m_serviceReference = reference;
		m_endpointDescription = endpointDescription;
	}

	@Override
	public ServiceReference getExportedService() {
		return m_serviceReference;
	}

	@Override
	public EndpointDescription getExportedEndpoint() {
		return m_endpointDescription;
	}

}
