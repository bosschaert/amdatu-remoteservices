package org.amdatu.remoteservices.remoteserviceadmin.impl;

import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.ImportReference;
import org.osgi.service.remoteserviceadmin.ImportRegistration;

public class ImportRegistrationImpl implements ImportRegistration {
	
	private final Throwable m_exception;
	private final ServiceRegistration m_proxyServiceRegistration;
	private ImportReference m_importReference;
	private final RemoteServiceAdminImpl m_remoteServiceAdmin;
	
	private ImportRegistrationImpl m_parent;
	private EndpointDescription m_endpointDescription;
	private ServiceReference m_proxyServiceReference;
	
	private boolean m_closed;
	
	private int m_instanceCount = 1;
	
	
	public ImportRegistrationImpl(ServiceReference proxyServiceReference, EndpointDescription endpointDescription, RemoteServiceAdminImpl remoteServiceAdmin, ServiceRegistration proxyServiceRegistration) {
		m_proxyServiceReference = proxyServiceReference;
		m_endpointDescription = endpointDescription;
		m_remoteServiceAdmin = remoteServiceAdmin;
		m_proxyServiceRegistration = proxyServiceRegistration;
		m_exception = null;
		m_parent = this;
	}

	public ImportRegistrationImpl(Throwable exception) {
		m_remoteServiceAdmin = null;
		m_importReference = null;
		m_proxyServiceRegistration = null;
		m_exception = exception;
		m_parent = this;
	}

	public ImportRegistrationImpl(ImportRegistrationImpl parent) {
		m_parent = parent;
		m_remoteServiceAdmin = m_parent.getRemoteServiceAdmin();
		m_exception = m_parent.getException();
		
		m_proxyServiceRegistration = null;
		
		m_parent.instanceAdded();
	}

	@Override
	public ImportReference getImportReference() {
		if (m_importReference == null) {
			m_importReference = new ImportReferenceImpl(m_proxyServiceReference, m_endpointDescription);
		}
		return m_importReference;
	}

	@Override
	public void close() {
		if (!m_closed) { 
			m_closed = true;
			
			m_remoteServiceAdmin.removeImport(this);
			
			m_parent.instanceClosed();
		}
	}
	
	@Override
	public Throwable getException() {
		if (!m_closed) {
			return m_exception;
		} else {
			return null;
		}
	}
	
	public ServiceRegistration getProxyServiceRegistration() {
		return m_proxyServiceRegistration;
	}
	
	public void instanceAdded() {
		++m_instanceCount;
	}
	
	public void instanceClosed() {
		--m_instanceCount;
		
		if (m_instanceCount <= 0) {
			synchronized (this) {
				if (m_proxyServiceRegistration != null) {
					m_proxyServiceRegistration.unregister();
				}
			}
		}
	}
	
	public RemoteServiceAdminImpl getRemoteServiceAdmin() {
		return m_remoteServiceAdmin;
	}
	
	public EndpointDescription getEndpoint() {
		return m_endpointDescription;
	}

}
