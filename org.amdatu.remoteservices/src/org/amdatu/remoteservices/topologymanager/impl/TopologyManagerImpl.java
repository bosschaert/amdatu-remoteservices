package org.amdatu.remoteservices.topologymanager.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.hooks.service.ListenerHook;
import org.osgi.service.log.LogService;
import org.osgi.service.remoteserviceadmin.EndpointDescription;
import org.osgi.service.remoteserviceadmin.EndpointListener;
import org.osgi.service.remoteserviceadmin.ExportRegistration;
import org.osgi.service.remoteserviceadmin.ImportRegistration;
import org.osgi.service.remoteserviceadmin.RemoteConstants;
import org.osgi.service.remoteserviceadmin.RemoteServiceAdmin;

public class TopologyManagerImpl implements EndpointListener, ListenerHook {

	// Dependency Manager injected fields
	private volatile BundleContext m_bundleContext;
	private volatile LogService m_log;
	
	private List<RemoteServiceAdmin> m_remoteServiceAdmins;
	
	private Map<ServiceReference, EndpointListener> m_endpointListeners;
	
	private Map<ServiceReference, Map<RemoteServiceAdmin, Collection<ExportRegistration>>> m_exportedServices;
	
	private ExecutorService m_executor = Executors.newFixedThreadPool(10);
	 
	private Map<String, List<EndpointDescription>> m_importPossibilities;
	private Map<String, List<ImportRegistration>> m_importedServices;
	private Map<String, ImportInterest> m_importInterests;
	
	private List<String> m_endpointListenerScope;
	private ServiceRegistration m_endpointListenerRegistration;
	private ServiceRegistration m_listenerHookRegistration;
	
	private final static String CLASS_NAME_EXPRESSION = ".*\\(" + Constants.OBJECTCLASS + "=([a-zA-Z_0-9.]+)\\).*";
	private final static Pattern CLASS_NAME_PATTERN = Pattern.compile(CLASS_NAME_EXPRESSION);
	
	// #TODO Taken (and updated) from CXF implementation, what should be done here?
	private final static Set<String> SYSTEM_PACKAGES;
	static {
		SYSTEM_PACKAGES = new HashSet<String>();
		SYSTEM_PACKAGES.add("org.osgi.service");
		SYSTEM_PACKAGES.add("org.apache.felix");
	}
	
	private class ImportInterest {
		int m_refs;
		
		public ImportInterest(String filter) {
			m_refs = 1;
		}
		
		public int addReference() {
			return ++m_refs;
		}
		
		public int removeReference() {
			return --m_refs;
		}
	}
	
	public TopologyManagerImpl() {
		m_remoteServiceAdmins = new ArrayList<RemoteServiceAdmin>();
		m_exportedServices = new LinkedHashMap<ServiceReference, Map<RemoteServiceAdmin,Collection<ExportRegistration>>>();
		m_endpointListeners = new HashMap<ServiceReference, EndpointListener>();
		
		m_importPossibilities = new HashMap<String, List<EndpointDescription>>();
		m_importedServices = new HashMap<String, List<ImportRegistration>>();
		
		m_endpointListenerScope = new ArrayList<String>();
		
		m_importInterests = new HashMap<String, TopologyManagerImpl.ImportInterest>();
	}
	
	public void start() {
		m_endpointListenerRegistration = m_bundleContext.registerService(EndpointListener.class.getName(), this, getEndpointListenerProperties());

		m_listenerHookRegistration = m_bundleContext.registerService(ListenerHook.class.getName(), this, null);
	}
	
	public void stop() {
		m_executor.shutdown();
		
		m_listenerHookRegistration.unregister();
		m_endpointListenerRegistration.unregister();
	}
	
	// Remote Service Admin handling
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void remoteServiceAdminAdded(RemoteServiceAdmin rsa) {
		m_log.log(LogService.LOG_INFO, "Adding Remote Service Admin.");
		synchronized (m_remoteServiceAdmins) {
			m_remoteServiceAdmins.add(rsa);
		}
		
		triggerExport(rsa);
		triggerImport(rsa);
	}
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void remoteServiceAdminRemoved(RemoteServiceAdmin rsa) {
		m_log.log(LogService.LOG_INFO, "Removing Remote Service Admin.");
		synchronized (m_remoteServiceAdmins) {
			m_remoteServiceAdmins.remove(rsa);
		}
		
		removeRemoteServiceAdmin(rsa);
	}
	
	/**
	 * Update all exported service to include this RemoteServiceAdmin.
	 * 
	 * @param rsa The RemoteServiceAdmin used to export the services.
	 */
	private void triggerExport(RemoteServiceAdmin rsa) {
		synchronized (m_exportedServices) {
			for (Entry<ServiceReference, Map<RemoteServiceAdmin, Collection<ExportRegistration>>> entry : m_exportedServices.entrySet()) {
				if (entry.getValue().containsKey(rsa)) {
					m_log.log(LogService.LOG_INFO, "Service already handled by this RemoteServiceAdmin");
				} else {
					m_log.log(LogService.LOG_INFO, "Service needs to be exported by this RemoteServiceAdmin");
					triggerExport(entry.getKey());
				}
			}
		}
	}
	
	/**
	 * Update the given RemoteServiceAdmin with all ImportPossibilities
	 * 
	 * @param rsa The RemoteServiceAdmin used to import the services.
	 */
	private void triggerImport(RemoteServiceAdmin rsa) {
		synchronized (m_importPossibilities) {
			Set<Entry<String, List<EndpointDescription>>> entries = m_importPossibilities.entrySet();
			for (Entry<String, List<EndpointDescription>> entry : entries) {
				triggerImport(entry.getKey());
			}
		}
	}
	
	/**
	 * Remove all exports belonging to the given RemoteServiceAdmin.
	 * Also notifies all EndpointListeners, thus implicitly also updates the discovery services.
	 * 
	 * @param rsa The RemoteServiceAdmin for which all exports must be removed.
	 */
	private void removeRemoteServiceAdmin(RemoteServiceAdmin rsa) {
		synchronized (m_exportedServices) {
			for (Entry<ServiceReference, Map<RemoteServiceAdmin, Collection<ExportRegistration>>> entry : m_exportedServices.entrySet()) {
				if (entry.getValue().containsKey(rsa)) {
					m_log.log(LogService.LOG_INFO, "Service handled by this RemoteServiceAdmin, needs to be removed");
					Collection<ExportRegistration> exportRegistrations = entry.getValue().get(rsa);
					notifyListenersOfRemoval(exportRegistrations);
					entry.getValue().remove(rsa);
				}
			}
		}
	}
	
	// Exported Interfaces handing
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void addExportedInterfaces(ServiceReference reference, Object service) {
		m_log.log(LogService.LOG_INFO, "Exported service added: " + reference.getProperty(RemoteConstants.SERVICE_EXPORTED_INTERFACES) + ".");

		synchronized (m_exportedServices) {
			m_exportedServices.put(reference, new LinkedHashMap<RemoteServiceAdmin, Collection<ExportRegistration>>());
		}
		
		triggerExport(reference);
	}
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void removeExportedInterfaces(ServiceReference reference, Object service) {
		m_log.log(LogService.LOG_INFO, "Exported service removed: " + reference.getProperty(RemoteConstants.SERVICE_EXPORTED_INTERFACES) + ".");
		
		removeExportedService(reference);
	}
	
	/**
	 * Exports all marked interfaces of the given reference, using the {@link RemoteConstants#SERVICE_EXPORTED_INTERFACES} property.
	 * Export are handled asynchronously using an {@link ExecutorService}
	 * 
	 * @param reference The reference to export the interfaces from.
	 */
	private void triggerExport(final ServiceReference reference) {
		m_executor.execute(new Runnable() {
			
			@Override
			public void run() {
				Map<RemoteServiceAdmin, Collection<ExportRegistration>> exports = null;
				
				synchronized (m_exportedServices) {
					exports = Collections.synchronizedMap(m_exportedServices.get(reference));
				}
				
				if (exports != null) {
					synchronized (m_remoteServiceAdmins) {
						for (RemoteServiceAdmin rsa : m_remoteServiceAdmins) {
							if (exports.containsKey(rsa)) {
								m_log.log(LogService.LOG_INFO, "Reference is already handled, will be ignored.");
							} else {
								Collection<ExportRegistration> exportRegistrations = rsa.exportService(reference, null);
								if (exportRegistrations == null) {
									// According to the spec the export cannot be null, an empty list is possible though.
									// So this should not happen!
									m_log.log(LogService.LOG_ERROR, "Error exporting reference.");
									exports.put(rsa, null);
								} else {
									exports.put(rsa, exportRegistrations);
									
									notifyListenersOfAddition(exportRegistrations);
								}
							}
						}
					}
				}
			}
		});
	}
	
	/**
	 * Remove all exports of the given reference, each export is closed and the EndpointListeners are notified.
	 * 
	 * @param reference The reference for which all exports must be removed.
	 */
	private void removeExportedService(ServiceReference reference) {
		synchronized (m_exportedServices) {
			if (m_exportedServices.containsKey(reference)) {
				Map<RemoteServiceAdmin, Collection<ExportRegistration>> exports = m_exportedServices.get(reference);
				for (Map.Entry<RemoteServiceAdmin, Collection<ExportRegistration>> entry : exports.entrySet()) {
					if (entry.getValue() != null) {
						Collection<ExportRegistration> registrations = entry.getValue();
						notifyListenersOfRemoval(registrations);
						for (ExportRegistration exportRegistration : registrations) {
							exportRegistration.close();
						}
					}
				}
				
				m_exportedServices.remove(reference);
			}
		}
	}
	
	// EndpointListener handling
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void endpointListenerAdded(ServiceReference reference, EndpointListener listener) {
		m_endpointListeners.put(reference, listener);
		
		informEndpointListener(reference);
	}
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void endpointListenerModified(ServiceReference reference, EndpointListener listener) {
		m_endpointListeners.put(reference, listener);
		
		informEndpointListener(reference);
	}
	
	@SuppressWarnings("unused") // Dependency Manager callback
	private void endpointListenerRemoved(ServiceReference reference, EndpointListener listener) {
		m_endpointListeners.remove(reference);
	}

	/**
	 * Inform listener of all existing exports.
	 * 
	 * @param reference Reference of the listener to inform.
	 */
	private void informEndpointListener(ServiceReference reference) {
		for (ServiceReference ref : m_exportedServices.keySet()) {
			Map<RemoteServiceAdmin, Collection<ExportRegistration>> rsas = m_exportedServices.get(ref);
			for (Collection<ExportRegistration> exports : rsas.values()) {
				notifyListenerOfAddition(reference, exports);
			}
		}
	}
	
	/**
	 * Notify all {@link EndpointListener}s of the added exports.
	 * 
	 * @param exports Collection with exports which have been added.
	 */
	private void notifyListenersOfAddition(Collection<ExportRegistration> exports) {
		for (ServiceReference reference : m_endpointListeners.keySet()) {
			if (reference.getProperty(EndpointListener.ENDPOINT_LISTENER_SCOPE) != null) {
				notifyListenerOfAddition(reference, exports);
			}
		}
	}
	
	/**
	 * Notify the referenced EndpointListener of the added exports.
	 * 
	 * @param reference The reference to the EndpointListener to be informed.
	 * @param exports Collection with exports which have been added.
	 */
	private void notifyListenerOfAddition(ServiceReference reference, Collection<ExportRegistration> exports) {
		EndpointListener listener = m_endpointListeners.get(reference);
		try {
			List<Filter> filters = normalizeScope(reference);
			
			for (ExportRegistration exportRegistration : exports) {
				Map<String, Object> properties = exportRegistration.getExportReference().getExportedEndpoint().getProperties();
				Dictionary<String, Object> dictionary = new Hashtable<String, Object>(properties);
				
				for (Filter filter : filters) {
					if (filter.match(dictionary)) {
						listener.endpointAdded(exportRegistration.getExportReference().getExportedEndpoint(), filter.toString());
					}
				}
			}
			
		} catch (InvalidSyntaxException e) {
			m_log.log(LogService.LOG_ERROR, "Error normalizing scope.", e);
		}
		
	}
	
	/**
	 * Notify all {@link EndpointListener}s of the removed exports.
	 * 
	 * @param exports Collection with exports which have been removed.
	 */
	private void notifyListenersOfRemoval(Collection<ExportRegistration> exports) {
		for (ServiceReference reference : m_endpointListeners.keySet()) {
			if (reference.getProperty(EndpointListener.ENDPOINT_LISTENER_SCOPE) != null) {
				notifyListenerOfRemoval(reference, exports);
			}
		}
	}
	
	/**
	 * Notify the referenced EndpointListener of the removed exports.
	 * 
	 * @param reference The reference to the EndpointListener to be informed.
	 * @param exports Collection with exports which have been removed.
	 */
	private void notifyListenerOfRemoval(ServiceReference reference, Collection<ExportRegistration> exports) {
		EndpointListener listener = m_endpointListeners.get(reference);
		try {
			List<Filter> filters = normalizeScope(reference);
			
			for (ExportRegistration exportRegistration : exports) {
				Map<String, Object> properties = exportRegistration.getExportReference().getExportedEndpoint().getProperties();
				Dictionary<String, Object> dictionary = new Hashtable<String, Object>(properties);
				
				for (Filter filter : filters) {
					if (filter.match(dictionary)) {
						listener.endpointRemoved(exportRegistration.getExportReference().getExportedEndpoint(), filter.toString());
					}
				}
			}
			
		} catch (InvalidSyntaxException e) {
			m_log.log(LogService.LOG_ERROR, "Error normalizing scope.", e);
		}
	}
	
	/**
	 * Normalize the {@link EndpointListener#ENDPOINT_LISTENER_SCOPE} of the given EndpointListener reference.
	 * Checks the scope and create a list of Filter elements out of it.
	 * 
	 * @param reference The reference to normalize the scope from.
	 * @return List with Filter objects for each element from the scope.
	 * @throws InvalidSyntaxException If the scope cannot be parsed to a Filter. 
	 */
	private List<Filter> normalizeScope(ServiceReference reference) throws InvalidSyntaxException {
		List<Filter> filters = new ArrayList<Filter>();
		
		Object scope = reference.getProperty(EndpointListener.ENDPOINT_LISTENER_SCOPE);
		if (scope instanceof String) {
			filters.add(m_bundleContext.createFilter((String) scope));
		} else if (scope instanceof String[]) {
			String[] scopes = (String[]) scope;
			for (String string : scopes) {
				filters.add(m_bundleContext.createFilter((String) string));
			}
		} else if (scope instanceof Collection<?>) {
			Collection<?> scopes = (Collection<?>) scope;
			for (Object object : scopes) {
				if (object instanceof String) {
					filters.add(m_bundleContext.createFilter((String) object));
				} else {
					m_log.log(LogService.LOG_INFO, "Scope is not a string, will be ignored.");
				}
			}
		}
		
		return filters;
	}
	
	// EndpointListener implementation
	
	@Override
	public void endpointAdded(EndpointDescription endpoint, String matchedFilter) {
		m_log.log(LogService.LOG_INFO, "Endpoint added with filter: " + matchedFilter);
		
		if (matchedFilter != null) {
			addImportableService(endpoint, matchedFilter);
		} else {
			m_log.log(LogService.LOG_ERROR, "Unable to handle endpoint, no matching filter provided.");
		}
	}

	@Override
	public void endpointRemoved(EndpointDescription endpoint, String matchedFilter) {
        m_log.log(LogService.LOG_INFO, "Endpoint removed with filter: " + matchedFilter);
		removeImportableService(endpoint, matchedFilter);
	}
	
	/**
	 * Adds the given endpoint and filter to the import possibilities.
	 * After adding an import is triggered for the matched filter.
	 * 
	 * @param endpoint the endpoint to add.
	 * @param matchedFilter the filter from which the original match has been done.
	 */
	private void addImportableService(EndpointDescription endpoint, String matchedFilter) {
		synchronized (m_importPossibilities) {
			List<EndpointDescription> possibilities = m_importPossibilities.get(matchedFilter);
			if (possibilities == null) {
				possibilities = new ArrayList<EndpointDescription>();
				m_importPossibilities.put(matchedFilter, possibilities);
			}
			
			if (!possibilities.contains(endpoint)) {
				possibilities.add(endpoint);
			}
		}
		
		synchronized (m_importedServices) {
			List<ImportRegistration> importRegistrations = m_importedServices.get(matchedFilter);
			if (importRegistrations == null) {
				importRegistrations = new ArrayList<ImportRegistration>();
				m_importedServices.put(matchedFilter, importRegistrations);
			}
        }
        
		triggerImport(matchedFilter);
	}
	
	/**
	 * Removed the given endpoint and filter from the import possibilities.
	 * After removing an import is triggered for the matched filter to remove imported services.
	 * 
	 * @param endpoint the endpoint to remove.
	 * @param matchedFilter the filter from which the original match has been done.
	 */
	private void removeImportableService(EndpointDescription endpoint, String matchedFilter) {
		synchronized (m_importPossibilities) {
			List<EndpointDescription> possibilities = m_importPossibilities.get(matchedFilter);
			if (possibilities != null) {
				while (possibilities.remove(endpoint));
			}
		}
		
		triggerImport(matchedFilter);
	}
	
	/**
	 * Triggers an import of the given filter.
	 * During an import existing imports are checked for availability, and removed if needed.
	 * New imports are created and added for new {@link EndpointDescription}s.
	 * 
	 * @param filter The filter of the matched Endpoints.
	 */
	private void triggerImport(final String filter) {
		m_executor.execute(new Runnable() {
			
			@Override
			public void run() {
				synchronized (m_importedServices) {
					synchronized(m_importPossibilities) {
						List<ImportRegistration> importRegistrations = m_importedServices.get(filter);
						List<EndpointDescription> endpoints = m_importPossibilities.get(filter);
						
						if (importRegistrations.size() > 0) {
							for (Iterator<ImportRegistration> iterator = importRegistrations.iterator(); iterator.hasNext();) {
								ImportRegistration importRegistration = (ImportRegistration) iterator.next();
								EndpointDescription importedEndpoint = importRegistration.getImportReference().getImportedEndpoint();
								
								if ((endpoints != null && !endpoints.contains(importedEndpoint)) || endpoints == null) {
									importRegistration.close();
									iterator.remove();
								}
								
							}
						}
						
						for (EndpointDescription endpointDescription : endpoints) {
							if (!importRegistrationsContainsEndpointDescription(importRegistrations, endpointDescription)) {
								synchronized (m_remoteServiceAdmins) {
									for (RemoteServiceAdmin rsa : m_remoteServiceAdmins) {
										ImportRegistration importRegistration = rsa.importService(endpointDescription);
										if (importRegistration != null && importRegistration.getException() == null) {
											importRegistrations.add(importRegistration);
										}
									}
								}
							}
						}
					}
				}
			}
		});
	}
	
	private boolean importRegistrationsContainsEndpointDescription(List<ImportRegistration> registrations, EndpointDescription endpointDescription) {
		boolean result = false;
		for (ImportRegistration reg : registrations) {
			if (reg.getImportReference().getImportedEndpoint().equals(endpointDescription)) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	private void extendEndpointListenerScope(String filter) {
		if (filter != null) {
			synchronized (m_endpointListenerScope) {
				m_endpointListenerScope.add(filter);
			}
			updateEndpointListenerRegistration();
		}
	}
	
	private void reduceEndpointListenerScope(String filter) {
		if (filter != null) {
			synchronized (m_endpointListenerScope) {
				m_endpointListenerScope.remove(filter);
			}
			
			updateEndpointListenerRegistration();
		}
	}
	
	private Properties getEndpointListenerProperties() {
		Properties p = new Properties();
		
		p.put(EndpointListener.ENDPOINT_LISTENER_SCOPE, m_endpointListenerScope);
		
		return p;
	}
	
	private void updateEndpointListenerRegistration() {
		m_endpointListenerRegistration.setProperties(getEndpointListenerProperties());
	}

	// ListenerHook implementation

	@Override
	public void added(@SuppressWarnings("rawtypes") Collection listeners) {
		for (Object listener : listeners) {
			ListenerInfo info = (ListenerInfo) listener;
			
			if (info.getBundleContext().getBundle().equals(m_bundleContext.getBundle())) {
				m_log.log(LogService.LOG_INFO, "Ignore requests from myself");
				continue;
			}
			
			// Exclude system packages?
			String className = getClassNameFromFilter(info.getFilter());
			if (info.getFilter() != null && !isClassExcluded(className)) {
				addServiceInterest(info.getFilter());
			}
		}
	}

	@Override
	public void removed(@SuppressWarnings("rawtypes") Collection listeners) {
		for (Object listener : listeners) {
			ListenerInfo info = (ListenerInfo) listener;
			
			if (info.getBundleContext().getBundle().equals(m_bundleContext.getBundle())) {
				m_log.log(LogService.LOG_INFO, "Ignore requests from myself");
				continue;
			}
			
			// Exclude system packages?
			if (info.getFilter() != null) {
				removeServiceInterest(info.getFilter());
			}
		}
	}
	
	private void addServiceInterest(String filter) {
		m_log.log(LogService.LOG_INFO, "Add service interest: " + filter);
		String extendedFilter = extendFilter(filter);
		synchronized (m_importInterests) {
			ImportInterest interest = m_importInterests.get(extendedFilter);
			if (interest != null) {
				interest.addReference();
			} else {
				m_importInterests.put(extendedFilter, new ImportInterest(extendedFilter));
				extendEndpointListenerScope(extendedFilter);
			}
		}
	}
	
	private void removeServiceInterest(String filter) {
		String extendedFilter = extendFilter(filter);
		
		synchronized (m_importInterests) {
			ImportInterest interest = m_importInterests.get(extendedFilter);
			if (interest != null) {
				if (interest.removeReference() <= 0) {
					reduceEndpointListenerScope(extendedFilter);
					m_importInterests.remove(extendedFilter);
					List<ImportRegistration> importRegistrations = m_importedServices.remove(extendedFilter);
					if (importRegistrations != null) {
						for (ImportRegistration importRegistration : importRegistrations) {
							if (importRegistration != null) {
								importRegistration.close();
							}
						}
					}
				}
			}
		}
	}
	
	private String extendFilter(String filter) {
		if (filter == null) {
			return "!(" + RemoteConstants.ENDPOINT_FRAMEWORK_UUID + "=" + getUUID(m_bundleContext) + ")";
		} else {
			return "(&" + filter + "(!(" + RemoteConstants.ENDPOINT_FRAMEWORK_UUID + "=" + getUUID(m_bundleContext) + ")))";
		}
	}
	
	private String getUUID(BundleContext bctx) {
        synchronized ("org.osgi.framework.uuid") {
            String uuid = bctx.getProperty("org.osgi.framework.uuid");
            if(uuid==null){
                uuid = UUID.randomUUID().toString();
                System.setProperty("org.osgi.framework.uuid", uuid);
            }
            return uuid;
        }
    }
	
	private String getClassNameFromFilter(String filter) {
        if (filter != null) {
            Matcher matcher = CLASS_NAME_PATTERN.matcher(filter);
            if (matcher.matches() && matcher.groupCount() >= 1) {
                return matcher.group(1);
            }
        }
        return null;
    }

    private boolean isClassExcluded(String className) {
        if (className == null) {
            return true;
        }

        for (String p : SYSTEM_PACKAGES) {
            if (className.startsWith(p)) {
                m_log.log(LogService.LOG_INFO, "Lookup for " + className + " is ignored");
                return true;
            }
        }
        return false;
    }
}
