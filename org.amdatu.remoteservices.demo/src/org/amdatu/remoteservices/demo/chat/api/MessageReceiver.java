package org.amdatu.remoteservices.demo.chat.api;

public interface MessageReceiver {

	void receive(String message);
}
