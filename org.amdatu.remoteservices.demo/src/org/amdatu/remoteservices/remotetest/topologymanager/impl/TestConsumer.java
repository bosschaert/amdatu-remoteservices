package org.amdatu.remoteservices.remotetest.topologymanager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.remoteservices.test.topologymanager.Test;
import org.osgi.framework.ServiceReference;

public class TestConsumer implements Runnable {

	private volatile Thread thread = null;
	private Map<ServiceReference, Test> m_tests = new HashMap<ServiceReference, Test>();
	
	public TestConsumer() {
	}
	
	public void start() {
		thread = new Thread(this, "TestConsumer Thread");
		thread.start();
	}
	
	public void stop() {
		thread = null;
	}
	
	public void add(ServiceReference reference, Test test) {
	    synchronized (m_tests) {
            m_tests.put(reference, test);
        }
	}
	
	public void remove(ServiceReference reference, Test test) {
        synchronized (m_tests) {
            m_tests.remove(reference);
        }
	}

	@Override
	public void run() {
		Thread thisThread = Thread.currentThread();
		while (thread == thisThread) {
			try {
			    synchronized (m_tests) {
                    for (Test test : m_tests.values()) {
                        test.printMessage("Consumer says hi");
                    }
                }
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
